package com.example.fvalerob.androidstreaming;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;


public class ConfigurarActivity extends AppCompatActivity{
    String stringCanal;
    String stringPresset;
    String stringIp;
    Integer stringPuerto;
    String stringUsuario;
    String stringPassword;
    Spinner spinnerCanal;
    Spinner spinnerPresset;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configurar);

        spinnerCanal = (Spinner) findViewById(R.id.spinnerCanal);
        spinnerPresset = (Spinner) findViewById(R.id.spinnerPresset);
        final EditText ip = (EditText) findViewById(R.id.ip);
        final EditText puerto = (EditText) findViewById(R.id.puerto);
        final EditText usuario = (EditText) findViewById(R.id.usuario);
        final EditText password = (EditText) findViewById(R.id.password);
        spinnerCanal.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {

                        Object item = parent.getItemAtPosition(pos);
                        System.out.println();     //prints the text in spinner item.
                        stringCanal = item.toString();

                    }
                    public void onNothingSelected(AdapterView<?> parent) {
                    }
                });
        spinnerPresset.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {

                        Object item = parent.getItemAtPosition(pos);
                        System.out.println(item.toString());     //prints the text in spinner item.
                        stringPresset = item.toString();

                    }
                    public void onNothingSelected(AdapterView<?> parent) {
                    }
                });

        final SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);

        //leeemos preferencias
        stringCanal = sp.getString("STRING_CANAL","");
        spinnerCanal.setPrompt(stringCanal);

        stringPresset = sp.getString("STRING_PRESSET","");
        spinnerPresset.setPrompt(stringPresset);

        stringIp = sp.getString("IP","192.168.0.1");
        ip.setText(stringIp);

        stringPuerto = sp.getInt("PUERTO",8080);
        puerto.setText(Integer.toString(stringPuerto));

        stringUsuario = sp.getString("USUARIO","valero");
        usuario.setText(stringUsuario);

        stringPassword = sp.getString("PASSWORD","1234");
        password.setText(stringPassword);

        Button guardar = (Button) findViewById(R.id.guardar);
        //onClickButton
        guardar.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {

                SharedPreferences.Editor editor = sp.edit();
                editor.putString("STRING_CANAL", stringCanal);
                editor.putString("STRING_PRESSET", stringPresset);
                editor.putString("IP", ip.getText().toString());
                editor.putInt("PUERTO", Integer.parseInt(puerto.getText().toString()));
                editor.putString("USUARIO", usuario.getText().toString());
                editor.putString("PASSWORD", password.getText().toString());

                editor.commit();
                finish();//Volvemos
            }
        });
    }

}

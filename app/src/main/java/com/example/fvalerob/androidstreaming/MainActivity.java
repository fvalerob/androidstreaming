package com.example.fvalerob.androidstreaming;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.SurfaceHolder;
import android.view.View;
import android.widget.Button;

import net.majorkernelpanic.streaming.Session;
import net.majorkernelpanic.streaming.SessionBuilder;
import net.majorkernelpanic.streaming.audio.AudioQuality;
import net.majorkernelpanic.streaming.gl.SurfaceView;
import net.majorkernelpanic.streaming.rtsp.RtspClient;
import net.majorkernelpanic.streaming.video.VideoQuality;


public class MainActivity extends AppCompatActivity implements SurfaceHolder.Callback, Session.Callback, RtspClient.Callback{
    SurfaceView mSurfaceView;
    Session mSession;
    SurfaceHolder mHolder;
    RtspClient mClient;
    Button configurar;
    Button comenzar;
    Button detener;
    Button acerca;
    String stringCanal;
    String stringPresset;
    String ip;
    Integer puerto;
    String usuario;
    String password;
    String calidadX;
    String calidadY;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        configurar = (Button)findViewById(R.id.configurar);
        comenzar = (Button)findViewById(R.id.comenzar);
        detener = (Button)findViewById(R.id.detener);
        detener.setEnabled(false);

        acerca = (Button)findViewById(R.id.acerca);

        mSurfaceView = (SurfaceView)findViewById(R.id.surfaceView);

        //Establemcemos el RTSPClient.
        mClient = new RtspClient();

        comenzar.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                //HABILITAR BOTON
                mClient.startStream();
                comenzar.setEnabled(false);
                detener.setEnabled(true);
            }
        });
        detener.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSession.stop();
                comenzar.setEnabled(true);
                detener.setEnabled(false);
            }
        });
        configurar.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                //HABILITAR BOTON
                Intent intent = new Intent(MainActivity.this, ConfigurarActivity.class);
                startActivity(intent);
            }
        });
        acerca.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                //HABILITAR BOTON
                Intent intent = new Intent(MainActivity.this, AcercaActivity.class);
                startActivity(intent);
            }
        });

    }
    public void recogerSharedPreferences(){
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        stringCanal = sp.getString("STRING_CANAL","/live/myStream");
        stringPresset = sp.getString("STRING_PRESSET","176 x 144 pixeles");
        //<item>176x144</item>
        String[] parts = stringPresset.split("x");
        calidadX = parts[0];
        calidadY = parts[1];

        ip = sp.getString("IP","172.20.10.2");
        puerto = sp.getInt("PUERTO",1935);
        usuario = sp.getString("USUARIO","valero");
        password = sp.getString("PASSWORD","1234");
    }
    @Override
    protected void onStart() {
        super.onStart();
        //Recogemos las SHARED_PREFERENCES
        recogerSharedPreferences();

        mHolder = mSurfaceView.getHolder();
        mHolder.addCallback(this);

        mSession = SessionBuilder.getInstance()
                .setCallback(this)
                .setSurfaceView(mSurfaceView)
                .setPreviewOrientation(90)
                .setContext(getApplicationContext())
                .setAudioEncoder(SessionBuilder.AUDIO_AAC)
                .setAudioQuality(new AudioQuality(8000, 16000))
                .setVideoEncoder(SessionBuilder.VIDEO_H264)
                //.setVideoQuality(new VideoQuality(176,144,20,500000))
                .setVideoQuality(new VideoQuality(Integer.parseInt(calidadX),Integer.parseInt(calidadY),20,500000))
                .build();


        mClient.setSession(mSession);
        mClient.setCallback(this);
        mSession.configure();

        mClient.setCredentials(usuario, password);
        mClient.setServerAddress(ip, puerto);
        mClient.setStreamPath(stringCanal);
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        mClient.release();
        mSession.release();
        mSurfaceView.getHolder().removeCallback(this);
        //mSession.stop();
    }

    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        //previ
        mSession.startPreview();
    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {

    }

    @Override
    public void onBitrateUpdate(long bitrate) {

    }

    @Override
    public void onSessionError(int reason, int streamType, Exception e) {

    }

    @Override
    public void onPreviewStarted() {

    }

    @Override
    public void onSessionConfigured() {

    }

    @Override
    public void onSessionStarted() {

    }

    @Override
    public void onSessionStopped() {

    }

    @Override
    public void onRtspUpdate(int message, Exception exception) {

    }
}
